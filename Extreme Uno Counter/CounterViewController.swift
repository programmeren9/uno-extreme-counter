//
//  ViewController.swift
//  Extreme Uno Counter
//
//  Created by Amber on 21/03/2020.
//  Copyright © 2020 Amber. All rights reserved.
//

import UIKit

import FirebaseCore
import FirebaseFirestore
import FirebaseFirestoreSwift

class CounterViewController: UIViewController {

    @IBOutlet weak var winnerName: UILabel!
    @IBOutlet weak var currentRoundPoints: UILabel!
    @IBOutlet weak var thisRoundPoints: UILabel!
    @IBOutlet weak var currentWinner: UILabel!
    @IBOutlet weak var roundNumber: UILabel!
    
    var currentPoints = 0
    var roundPoints = 0
    var round = 1
    
    var userName: String? = nil
    
    var db: Firestore!
    
    let network: NetworkManager = NetworkManager.sharedInstance
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //Init for database
        let settings = FirestoreSettings()
        Firestore.firestore().settings = settings
        db = Firestore.firestore()
        
        thisRoundPoints.text = String(currentPoints)
        currentRoundPoints.text = String(roundPoints)
        currentWinner.text = userName
        roundNumber.text = String(round)
        
        //Check if network is unreachable
        network.reachability.whenUnreachable = { network in
            print("Network is unreachable")
            
            //Send alert when network is unreachable to inform user that the data can not be saved
            let alert = UIAlertController(title: "No internet", message: "There is no internet connection, data will not be saved! Please try again.", preferredStyle: .alert)
            
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
                  switch action.style{
                  case .default:
                        print("default")

                  case .cancel:
                        print("cancel")

                  case .destructive:
                        print("destructive")


                  @unknown default:
                    fatalError()
                }}))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    //Multiple functions for the points counter
    @IBAction func numberCardPressed(_ sender: UIButton) {
        currentPoints += Int(sender.currentTitle!)!
        thisRoundPoints.text = String(currentPoints)
    }
    
    @IBAction func resetPressed(_ sender: UIButton) {
        currentPoints = 0
        thisRoundPoints.text = String(currentPoints)
    }
    
    @IBAction func savePointsPressed(_ sender: UIButton) {
        addDataToFirebaseWhenOnline()
        
        roundPoints = 0
        currentPoints = 0
        round = 1
        
        updateLabels()
    }
    
    @IBAction func addScorePressed(_ sender: UIButton) {
        roundPoints += currentPoints
        currentPoints = 0
        round += 1
        
        updateLabels()
    }
    
    func updateLabels() {
        roundNumber.text = String(round)
        thisRoundPoints.text = String(currentPoints)
        currentRoundPoints.text = String(roundPoints)
    }
    
    //Add score and name to database when network is available
    func addDataToFirebaseWhenOnline() {
        NetworkManager.isReachable { network in
            print("Network is available")
            self.addDataToFirebase()
        }
    }
    
    //Add score and name to firebase database
    func addDataToFirebase() {
        var ref: DocumentReference? = nil
        
        ref = db.collection("winner").addDocument(data: [
            "name": currentWinner.text!,
            "points": currentRoundPoints.text!
        ]) { err in
            if let err = err {
                print("Error adding document: \(err)")
            } else {
                print("Document added with ID: \(ref!.documentID)")
            }
        }
    }
}

