//
//  ApplyViewController.swift
//  Extreme Uno Counter
//
//  Created by Amber on 23/03/2020.
//  Copyright © 2020 Amber. All rights reserved.
//

import Foundation
import UIKit
import CoreLocation
import UserNotifications

class ApplyViewController: UIViewController, CLLocationManagerDelegate {
    
    @IBOutlet weak var startButton: UIButton!
    @IBOutlet weak var nameTextField: UITextField!
    
    let locationManager = CLLocationManager()
    let notificationCenter = UNUserNotificationCenter.current()
    let options: UNAuthorizationOptions = [.alert, .sound, .badge]
    //Set region for my home location for now
    let geofenceRegion = CLCircularRegion(center: CLLocationCoordinate2DMake(51.959210, 4.563690), radius: 10, identifier: "Home")
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //Ask user for authorization of using the location always
        locationManager.delegate = self
        locationManager.requestAlwaysAuthorization()
        locationManager.startUpdatingLocation()
        locationManager.startMonitoringSignificantLocationChanges()
        locationManager.allowsBackgroundLocationUpdates = true
        
        //Set if user gets notified when region entered or when region exits
        geofenceRegion.notifyOnEntry = true
        geofenceRegion.notifyOnExit = false
        
        locationManager.startMonitoring(for: geofenceRegion)
        
        //Get the notification settings of the user
        notificationCenter.getNotificationSettings{ settings in
            print("Notification settings: \(settings)")
            
            guard settings.authorizationStatus == .authorized else { return }
            DispatchQueue.main.async {
              UIApplication.shared.registerForRemoteNotifications()
            }
        }
    }
    
    //Send value of input text to next viewcontroller
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let counterViewController = segue.destination as? CounterViewController {
            counterViewController.userName = nameTextField.text
        }
    }
    
    //Print new location when location of user changes
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if let location = locations.first {
            print("Found user's location: \(location)")
        }
    }
    
    //Print error when users location cannot be found
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
           print("Failed to find user's location: \(error.localizedDescription)")
    }
}
