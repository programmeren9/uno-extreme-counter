//
//  AppDelegate.swift
//  Extreme Uno Counter
//
//  Created by Amber on 21/03/2020.
//  Copyright © 2020 Amber. All rights reserved.
//

import UIKit
import Firebase
import UserNotifications
import CoreLocation

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    var locationManager: CLLocationManager?
    
    let notificationCenter = UNUserNotificationCenter.current()

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        //Init firebase and database
        FirebaseApp.configure()
        let db = Firestore.firestore()
        print(db)
        
        //Init locationManager
        locationManager = CLLocationManager()
        locationManager?.delegate = self
        
        //Request authorization to send notifications
        notificationCenter.requestAuthorization(options:
        [.badge,.alert,.sound])
          {
              (sucess,error) in
              if error != nil {
                  print("Error Found, \(error?.localizedDescription ?? "")")

              } else {
                  print("Authorized by the user")
              }
          }

        notificationCenter.delegate = self as? UNUserNotificationCenterDelegate
        
        return true
    }

    // MARK: UISceneSession Lifecycle

    func application(_ application: UIApplication, configurationForConnecting connectingSceneSession: UISceneSession, options: UIScene.ConnectionOptions) -> UISceneConfiguration {
        // Called when a new scene session is being created.
        // Use this method to select a configuration to create the new scene with.
        return UISceneConfiguration(name: "Default Configuration", sessionRole: connectingSceneSession.role)
    }

    func application(_ application: UIApplication, didDiscardSceneSessions sceneSessions: Set<UISceneSession>) {
        // Called when the user discards a scene session.
        // If any sessions were discarded while the application was not running, this will be called shortly after application:didFinishLaunchingWithOptions.
        // Use this method to release any resources that were specific to the discarded scenes, as they will not return.
    }
    
    //Register for remote notifications
    func application(
      _ application: UIApplication,
      didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data
    ) {
      let tokenParts = deviceToken.map { data in String(format: "%02.2hhx", data) }
      let token = tokenParts.joined()
      print("Device Token: \(token)")
    }

    //When registering for remote notifications fail
    func application(
      _ application: UIApplication,
      didFailToRegisterForRemoteNotificationsWithError error: Error) {
      print("Failed to register: \(error)")
    }
    
    func handleEvent(forRegion region: CLRegion!) {
        // Here will be a notification but due to apple I can't test this so for now it will print something
        print("Wanna play uno? It will be fun!")
        
        //Send a notification on location trigger, location region is set inside ApplyViewController
        let content = UNMutableNotificationContent()
        content.title = "Play Uno"
        content.body = "Do you wanna play Uno? It will be fun!"
        content.sound = UNNotificationSound.default
        content.badge = 1
        
        //Trigger notification when user enters region
        let trigger = UNLocationNotificationTrigger(region: region, repeats: false)
        let request = UNNotificationRequest(identifier: "Home", content: content, trigger: trigger)
        
        //Add notification to the notification center
        notificationCenter.add(request) { error in
            if let error = error {
                print("Error: \(error.localizedDescription)")
            } else {
                print("Notification was successfully sent to user")
            }
        }
    }

}

extension AppDelegate: CLLocationManagerDelegate {
    //Called when user exits a region
    func locationManager(_ manager: CLLocationManager, didExitRegion region: CLRegion) {
        if region is CLCircularRegion {
            self.handleEvent(forRegion: region)
        }
    }
    
    //Called when user enters a region
    func locationManager(_ manager: CLLocationManager, didEnterRegion region: CLRegion) {
        if region is CLCircularRegion {
            self.handleEvent(forRegion: region)
        }
    }
}
